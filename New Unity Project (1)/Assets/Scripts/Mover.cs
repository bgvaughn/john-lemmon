﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Mover : MonoBehaviour   
{
    private float speed;
    // Start is called before the first frame update
    void Start()
    {
     speed = 15;
     GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(collision.gameObject, 0f); //enemy
            Destroy(gameObject, 0f); //bullet
        }
        else
        {
            Destroy(gameObject, 0f);
        }
      
    }
}
